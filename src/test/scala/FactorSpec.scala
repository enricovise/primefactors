import org.scalatest.funspec.AnyFunSpec

class FactorSpec extends AnyFunSpec {

  describe("A factor") {
    it("should be a positive number") {
      assertThrows[Exception] {
        new Factor(0)
      }
    }

    describe("when prime") {
      it("should be marked distinctively") {
        assert(new Factor(3).isPrime)
      }
    }

    describe("when not prime") {
      it("should be marked distinctively") {
        assert(!new Factor(4).isPrime)
      }
    }

    describe("when 1") {
      it("should resolve into no factors") {
        assert(new Factor(1).factorize() == Nil)
      }
    }

    describe("when 2") {
      it("should resolve in itself") {
        assert(new Factor(2).factorize() == List(2))
      }
    }

    describe("when 3") {
      it("should resolve in itself") {
        assert(new Factor(3).factorize() == List(3))
      }
    }

    describe("when 4") {
      it("should resolve in (2, 2))") {
        assert(new Factor(4).factorize() == List(2, 2))
      }
    }

    describe("when 5") {
      it("should resolve in itself)") {
        assert(new Factor(5).factorize() == List(5))
      }
    }

    describe("when 6") {
      it("should resolve in (2, 3))") {
        assert(new Factor(6).factorize() == List(2, 3))
      }
    }

    describe("when 24") {
      it("should resolve in (2, 2, 2, 3))") {
        assert(new Factor(24).factorize() == List(2, 2, 2, 3))
      }
    }

    describe("when 69") {
      it("should resolve in (3, 23))") {
        assert(new Factor(69).factorize() == List(3, 23))
      }
    }

    describe("when 174") {
      it("should resolve in (2, 3, 29))") {
        assert(new Factor(174).factorize() == List(2, 3, 29))
      }
    }
  }
}
