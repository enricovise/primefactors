class Factor(number: Int = 1) {

  if (this.number <= 0) {
    throw new Exception("Invalid number")
  }

  def isPrime: Boolean = {

    for (n <- 2 until this.number) {
      if (this.number % n == 0) {
        return false
      }
    }

    true
  }

  def factorize(divisor: Int = 2): List[Int] = {

    if (this.number <= 1) {
      Nil
    }
    else if (this.number % divisor > 0) {
      this.factorize(divisor + 1)
    }
    else {
      divisor +: new Factor(this.number / divisor).factorize()
    }

  }

}
